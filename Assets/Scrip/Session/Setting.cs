﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Setting : MonoBehaviour
{
    //no necesito void stup y loop
    public static Setting setting;//requiero que mnatenga la informacion siendo clase statica y acceder por atributos
    [SerializeField]
    private double containerVolume; //solo aqui cambio el valor del volumen del tanque
    //forma 1: lenguaje de sintaxis
    public double _containerVolume { set { this.containerVolume = value; } get { return containerVolume; } }
    private double volumen;
    public double _volumem { set { this.volumen = value; } get { return volumen; } }

    ////forma 2: creando funciones
    //public double getContainerVolumen()
    //{
    //    return containerVolume;
    //}
    //public void setContainerVolumen(double valor)
    //{
    //    containerVolume = valor;
    //}
    
    private void Awake()
    {
        //inicializa antes de star
        if (Setting.setting == null) //verifica que el objeto se encuentre en nulo
        {
            Setting.setting = this; //se asigna el obj seting y this apunta a si mismo
        }
        else
        {
            if(Setting.setting != this) //destruye el objeto
            {
                Destroy(this.gameObject);
            }
        }
        DontDestroyOnLoad(this.gameObject);
        Debug.Log(containerVolume);
    }
}