﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankFilling : MonoBehaviour
{
    [SerializeField]
    private GameObject water; // asignacion y declaracion del objeto
    [SerializeField]
    private Vector3 position; //toma posicion del vector water
    private double percent; //porcentaje del llenado en el que esta el recipiente
    private double heigth; //valor del volumen que esta recibiendo el recipiente
    [SerializeField]
    private double velocity; //aparece en el inspector velocidad con la que va aarecer el llenado del tanque
    private double volumen;
    private double auxiliar;

    // Start is called before the first frame update
    void Start()
    {
        position = water.transform.localPosition;//vector de posicion del recipiente, tomo las cordenadas del plano interno de agua 
    }

    // Update is called once per frame
    void Update()
    {
        Filling();   
    }

    private void Filling()
    {
        volumen = Setting.setting._volumem; // asignado el valor de volumen
        if(position.z < 0)
        {
            auxiliar = -volumen;
        }
        else if (position.z > 0)
        {
            auxiliar = volumen;
        }

        Debug.Log("volumen:" + volumen);
        percent = (auxiliar * 100) / Setting.setting._containerVolume;  //regla de 3: volumen%/capacidad
        heigth = (percent * 0.0485f) / 100; //* por un factor, tambien podria estar definido por setting
        Debug.Log("percent:" + percent+ "heigth:" + heigth);
        float scaley = System.Convert.ToSingle(position.z + heigth * velocity);//variable local ojo con float hay que transformas
        water.transform.localScale = new Vector3(water.transform.localScale.x, water.transform.localScale.y, scaley);//afectando la escala del objeto
        //float transformZ = System.Convert.ToSingle(position.z + heigth * velocity);
        //water.transform.localPosition = new Vector3(position.x, position.y, transformZ);
    }
}
